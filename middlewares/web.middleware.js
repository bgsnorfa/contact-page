const { User } = require("../models");

module.exports = {
    verifyWebSession: async (req, res, next) => {
        if (req.session.userId) {
            let user = await User.findByPk(req.session.userId);
            if (user) {
                next();
            } else {
                return res.redirect("/login");
            }
        } else {
            return res.redirect("/login");
        }
    },
    redirectIfSigned: async (req, res, next) => {
        if (!req.session.userId) {
            next();
        } else {
            res.redirect("/profile");
        }
    }
};