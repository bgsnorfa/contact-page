const express = require("express");
const session = require("express-session");
const cookieParser = require("cookie-parser");
const Sequelize = require("sequelize");
const basicAuth = require("express-basic-auth");

const SequelizeStore = require("connect-session-sequelize")(session.Store);

const dotenv = require('dotenv');
dotenv.config();

const swaggerUI = require("swagger-ui-express");

const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/config/config.json')[env];

const sequelize = new Sequelize(config.database, config.username, config.password, {
    dialect: config.dialect
});

const router = require("./routes");

const swaggerJSON = require("./docs/swagger/swagger.json");

const app = express();

app.use(session({
    secret: "secret",
    store: new SequelizeStore({
        db: sequelize
    }),
    saveUninitialized:true,
    resave: false
}));

sequelize.sync();

app.use(cookieParser());

app.set('view engine', 'ejs');

const port = "3000";

app.use('/docs', basicAuth({
    users: { 'admin': 'admin' },
    challenge: true
}), swaggerUI.serve, swaggerUI.setup(swaggerJSON));
app.use(router);

app.listen(port, () => {
    console.log(`App listening on port: ${port}`);
})