const profileControllerV1 = require("../../../controllers/api/v1/profile.controller");
const { verifyJWT } = require("../../../middlewares/api.middleware");
const bodyParser = require("body-parser");

const jsonParser = bodyParser.json();
const router = require("express").Router();

router.get("", jsonParser, verifyJWT, profileControllerV1.index);
router.post("", jsonParser, verifyJWT, profileControllerV1.create);
router.put("/:id/edit", jsonParser, verifyJWT, profileControllerV1.edit);
router.delete("/:id/delete/", jsonParser, verifyJWT, profileControllerV1.delete);
router.post("/:id/phone", jsonParser, verifyJWT, profileControllerV1.addPhone);
router.put("/:id/phone/:phoneId/edit", jsonParser, verifyJWT, profileControllerV1.editPhone);
router.delete("/:id/phone/:phoneId/delete", jsonParser, verifyJWT, profileControllerV1.deletePhone);

module.exports = router;