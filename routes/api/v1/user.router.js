const userControllerV1 = require("../../../controllers/api/v1/user.controller");
const { verifyJWT } = require("../../../middlewares/api.middleware");
const bodyParser = require("body-parser");

const jsonParser = bodyParser.json();
const router = require("express").Router();

router.post("/login", jsonParser, userControllerV1.login);
router.post("/register", jsonParser, userControllerV1.register);
router.get("/verify", jsonParser, verifyJWT, userControllerV1.verify);

module.exports = router;