const router = require("express").Router();
const bodyParser = require("body-parser");
const userController = require("../../controllers/api/user.controller");
const { verifyJWT } = require("../../middlewares/api.middleware");

const urlEncoded = bodyParser.urlencoded({ extended: false });

router.post("/login", urlEncoded, userController.login);
router.post("/register", urlEncoded, userController.register);
router.get("/logout", urlEncoded, userController.logout);

module.exports = router;