const router = require("express").Router();
require("express-group-routes");
const bodyParser = require("body-parser");
const profileController = require("../../controllers/api/profile.controller");

const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });

router.post("", urlEncoded, profileController.create);
router.post("/edit", urlEncoded, profileController.edit);
router.get("/delete/:id", profileController.delete);
router.post("/phone", urlEncoded, profileController.addPhone);
router.post("/phone/:id/edit", urlEncoded, profileController.editPhone);
router.get("/phone/:id/delete", urlEncoded, profileController.deletePhone);

module.exports = router;