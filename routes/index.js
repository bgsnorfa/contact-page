const router = require("express").Router();
const homeRouter = require("./home.router");
const profileRouter = require("./profile.router");

const apiUserRouter = require("./api/user.router");
const apiProfileRouter = require("./api/profile.router");

const apiV1UserRouter = require("./api/v1/user.router");
const apiV1ProfileRouter = require("./api/v1/profile.router");

// MVC
router.use("/", homeRouter);
router.use("/profile", profileRouter);

// MCR
router.use("/api/user", apiUserRouter);
router.use("/api/profile", apiProfileRouter);

// API
router.group("/api/v1", route => {
    route.use("/user", apiV1UserRouter);
    route.use("/profile", apiV1ProfileRouter);
});

module.exports = router;