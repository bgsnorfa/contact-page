const { Profile, Phone } = require("../../models");

module.exports = {
    create: async (req, res) => {
        try {
            let profile = await Profile.create({
                userId: req.session.userId,
                name: req.body.name
            });

            res.redirect('/');
        } catch (error) {
            res.render('profiles/create', {
                error: error
            });
        }
    },
    edit: async (req, res) => {
        try {
            let profile = await Profile.findByPk(req.body.id);
            profile.name = req.body.name ? req.body.name : profile.name;
            await profile.save();

            res.redirect('/');
        } catch (error) {
            res.render('profiles/edit', {
                error: error
            });
        }
    },
    delete: async (req, res) => {
        try {
            const count = await Profile.destroy({
                where: { id: req.params.id }
            });

            if (count > 0) {
                return res.redirect('/');
            } else {
                return res.redirect('/');
            }
        } catch (err) {
            return res.redirect('/');
        }
    },
    addPhone: async (req, res) => {
        try {
            let phone = await Phone.create({
                profileId: parseInt(req.body.profileId),
                data: req.body.data
            });

            res.redirect(`/profile/show/${req.body.profileId}`);
        } catch (error) {
            res.render('profiles/phones/create', {
                error: error,
                profile: Profile.findByPk(req.body.profileId)
            });
        }
    },
    editPhone: async (req, res) => {
        try {
            let phone = await Phone.findByPk(req.body.phoneId);
            phone.data = req.body.data ? req.body.data : phone.data;
            phone.isActive = req.body.isActive !== undefined
            await phone.save();

            res.redirect(`/profile/show/${req.body.profileId}`);
        } catch (error) {
            res.render('profiles/phones/edit', {
                session: req.session,
                phone: phone,
                error: error
            });
        }
    },
    deletePhone: async (req, res) => {
        try {
            let phone = await Phone.findByPk(req.params.id);
            let profileId = phone.profileId;
            const count = await phone.destroy();

            if (count > 0) {
                return res.redirect(`/profile/show/${profileId}`);
            } else {
                return res.redirect(`/profile/show/${profileId}`);
            }
        } catch (err) {
            return res.redirect('/');
        }
    }
};