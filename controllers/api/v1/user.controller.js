const { User } = require("../../../models");
const { error, success } = require("../../../utils/response/json.utils");

module.exports = {
    login: async (req, res) => {
        let message = 'Login failed, username/password does not match';

        try {
            let user = await User.findOne({
                where: { username: req.body.username }
            });

            user.validPasswordPromise(req.body.password)
                .then(resolve => {
                    return success(res, 200, 'Login success', {
                        token: user.generateToken()
                    });
                })
                .catch(reject => {
                    return error(res, 400, message, {});
                });

        } catch (err) {
            console.log(err);
            return error(res, 400, err.message, {});
        }
    },
    register: async (req, res) => {
        try {
            let user = await User.create({
                username: req.body.username,
                password: req.body.password,
            });

            return success(res, 201, 'user created');
        } catch (err) {
            return error(res, 400, err.message, {});
        }
    },
    verify: async (req, res) => {
        return success(res, 200, 'OK');
    }
};