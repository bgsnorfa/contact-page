const { success, error } = require("../../../utils/response/json.utils");
const { Profile, Phone, User } = require("../../../models");

module.exports = {
    index: async (req, res) => {
        let whereQuery = req.user.isAdmin ? {} : { userId: req.user.id };

        let orderQuery = [
            ['updatedAt', 'desc'],
            ['createdAt', 'desc']
        ];

        let includeQuery = [
            { model: Phone },
            { model: User, attributes: ['id', 'username'] }
        ];

        Profile.findAll({
            where: whereQuery,
            order: orderQuery,
            include: includeQuery
        })
        .then(profiles => {
            return success(res, 200, 'profiles', profiles);
        });
    },
    create: async (req, res) => {
        try {
            let profile = await Profile.create({
                userId: req.user.id,
                name: req.body.name
            });

            return success(res, 201, 'profile created', profile);
        } catch (err) {
            return error(res, 400, err.message, {});
        }
    },
    edit: async (req, res) => {
        try {
            let profile = await Profile.findByPk(req.params.id);
            profile.name = req.body.name ? req.body.name : profile.name;
            await profile.save();

            return success(res, 200, 'profile updated', profile);
        } catch (err) {
            return error(res, 400, err.message, {});
        }
    },
    delete: async (req, res) => {
        try {
            const count = await Profile.destroy({
                where: { id: req.params.id }
            });

            if (count > 0) {
                return success(res, 200, 'profile deleted');
            } else {
                return error(res, 400, 'error deleting profile');
            }
        } catch (err) {
            return error(res, 400, 'error deleting profile');
        }
    },
    addPhone: async (req, res) => {
        try {
            let phone = await Phone.create({
                profileId: parseInt(req.params.id),
                data: req.body.data
            });

            return success(res, 201, 'phone created', phone);
        } catch (err) {
            return error(res, 400, err.message, {});
        }
    },
    editPhone: async (req, res) => {
        try {
            let phone = await Phone.findByPk(req.params.phoneId);
            phone.data = req.body.data ? req.body.data : phone.data;
            phone.isActive = req.body.isActive ? true : false
            await phone.save();

            return success(res, 200, 'phone updated', phone);
        } catch (error) {
            return error(res, 400, err.message, {});
        }
    },
    deletePhone: async (req, res) => {
        try {
            let phone = await Phone.findByPk(req.params.phoneId);
            await phone.destroy();

            return success(res, 200, 'phone deleted');
        } catch (err) {
            return error(res, 400, 'error deleting phone');
        }
    }
};