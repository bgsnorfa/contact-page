const { Profile, Phone, User } = require("../models");

module.exports = {
    index: async (req, res) => {
        let whereQuery = req.session.isAdmin ? {} : { userId: req.session.userId };

        let orderQuery = [
            ['updatedAt', 'desc'],
            ['createdAt', 'desc']
        ];

        let includeQuery = [
            { model: Phone },
            { model: User }
        ];

        Profile.findAll({
            where: whereQuery,
            order: orderQuery,
            include: includeQuery
        })
        .then(profiles => {
            res.render('profiles/index', {
                session: req.session,
                profiles: profiles
            });
        });
    },
    create: async (req, res) => {
        res.render('profiles/create', {
            session: req.session
        });
    },
    edit: async (req, res) => {
        Profile.findByPk(req.params.id)
        .then(profile => {
            res.render('profiles/edit', {
                session: req.session,
                profile: profile
            });
        });
    },
    show: async (req, res) => {
        Profile.findByPk(req.params.id, {
            include: Phone
        })
        .then(profile => {
            res.render('profiles/show', {
                session: req.session,
                profile: profile
            });
        });
    },
    createPhone: (req, res) => {
        Profile.findByPk(req.params.id)
        .then(profile => {
            res.render('profiles/phones/create', {
                session: req.session,
                profile: profile
            });
        });
    },
    editPhone: async (req, res) => {
        Phone.findByPk(req.params.phoneId, {
            include: [
                { model: Profile }
            ]
        })
        .then(phone => {
            res.render('profiles/phones/edit', {
                session: req.session,
                phone: phone
            });
        });
    }
};
