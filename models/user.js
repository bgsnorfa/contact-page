'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.Profile, {
        foreignKey: 'userId'
      });
    }
  }
  User.init({
    username: {
      type: DataTypes.STRING,
      validate: {
        len: 5,
        isAlphanumeric: true,
        isUnique: (value, next) => {
          User.findAll({
            where: { username: value }
          })
          .then((user) => {
            if (user.length != 0) {
              next(new Error('username already in use'));
            }
            next();
          });
        }
      },
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        is: /^[0-9a-zA-Z]{6}$/i
      },
      allowNull: false
    },
    isAdmin: {
      type: DataTypes.BOOLEAN
    }
  }, {
    sequelize,
    modelName: 'User',
  });

  User.addHook('afterValidate', (user, options) => {
    const salt = bcrypt.genSaltSync();
    user.password = bcrypt.hashSync(user.password, salt);
  });

  User.prototype.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
  }

  User.prototype.validPasswordPromise = function(password) {
    return new Promise((resolve, reject) => {
      if (bcrypt.compareSync(password, this.password)) {
        resolve('password is valid');
      } else {
        reject('password invalid');
      }
    });
  }

  User.prototype.generateToken = function() {
    const payload = {
      id: this.id,
      username: this.username,
      isAdmin: this.isAdmin
    };

    const token = jwt.sign(payload, process.env.JWT_SECRET || 'secret', { expiresIn: '1 day' });
    return token;
  }

  return User;
};